﻿using MyCollection.Application.Common.Interfaces.Services;

namespace MyCollection.Infrastructure.Services
{
    public class DateTimeProvider : IDateTimeProvider
    {
        public DateTime UtcNow => DateTime.UtcNow;
    }
}
