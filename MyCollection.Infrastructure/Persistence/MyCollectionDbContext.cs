﻿using Microsoft.EntityFrameworkCore;

using MyCollection.Domain.MenuAggregate;
using MyCollection.Domain.MenuAggregate.Entities;

namespace MyCollection.Infrastructure.Persistence
{
    public class MyCollectionDbContext : DbContext
    {
        public MyCollectionDbContext(DbContextOptions<MyCollectionDbContext> options) : base(options) { }

        public DbSet<Menu> Menus { get; set; } = null!;
        public DbSet<MenuSection> MenuSections { get; set; } = null!;
        public DbSet<MenuItem> MenuItems { get; set; } = null!;

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfigurationsFromAssembly(typeof(MyCollectionDbContext).Assembly);
            base.OnModelCreating(modelBuilder);
        }

    }
}
