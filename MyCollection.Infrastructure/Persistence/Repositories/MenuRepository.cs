﻿using MyCollection.Application.Common.Interfaces.Persistence;
using MyCollection.Domain.MenuAggregate;

namespace MyCollection.Infrastructure.Persistence.Repositories
{
    public class MenuRepository : IMenuRepository
    {
        private static readonly List<Menu> _menus = new List<Menu>();
        public void Add(Menu menu)
        {
            _menus.Add(menu);
        }
    }
}
