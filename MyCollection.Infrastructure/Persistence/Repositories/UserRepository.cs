﻿using MyCollection.Application.Common.Interfaces.Persistence;
using MyCollection.Domain.UserAggregate.Entities;

namespace MyCollection.Infrastructure.Persistence.Repositories
{
    public class UserRepository : IUserRepository
    {
        private static readonly List<User> _users = new List<User>();

        public void Add(User user)
        {
            _users.Add(user);
        }

        public User? GetUserByUsername(string username)
        {
            return _users.SingleOrDefault(x => x.Username == username);
        }
    }
}
