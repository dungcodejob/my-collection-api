﻿namespace MyCollection.Contracts.Authentication
{
    public record RegisterRequestDto(
        string FirstName,
        string LastName,
        string Password,
        string Username);
}
