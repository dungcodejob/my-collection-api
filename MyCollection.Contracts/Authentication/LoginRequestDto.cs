﻿namespace MyCollection.Contracts.Authentication
{
    public record LoginRequestDto(
        string Password,
        string Username);
}
