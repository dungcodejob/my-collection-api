﻿namespace MyCollection.Contracts.Authentication
{
    public record AuthenticationResponseDto(
        Guid Id,
        string FirstName,
        string LastName,
        string Username,
        string Token);

}
