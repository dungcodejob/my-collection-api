﻿using MyCollection.Domain.Common.Models;

namespace MyCollection.Domain.Common.ValueObjects
{
    public sealed class AverageRating : ValueObject
    {
        private AverageRating(double value, int numberRatings)
        {
            Value = value;
            NumberRatings = numberRatings;
        }

        public double Value { get; private set; }
        public int NumberRatings { get; private set; }

        public static AverageRating CreateNew()
        {
            return new AverageRating(0, 0);
        }

        public void AddNewRating(Rating rating)
        {
            Value = ((Value * NumberRatings) + rating.Value) / ++NumberRatings;
        }

        public void RemoveRating(Rating rating)
        {
            Value = ((Value * NumberRatings) - rating.Value) / --NumberRatings;
        }


        public override IEnumerable<object> GetEqualytyComponents()
        {
            yield return Value;
            yield return NumberRatings;
        }
    }
}
