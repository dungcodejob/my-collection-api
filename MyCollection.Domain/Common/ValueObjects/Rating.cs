﻿using MyCollection.Domain.Common.Models;

namespace MyCollection.Domain.Common.ValueObjects
{
    public sealed class Rating : ValueObject
    {
        private Rating(double value)
        {
            Value = value;
        }

        public double Value { get; private set; }

        public Rating Create()
        {
            return new Rating(Value);
        }

        public override IEnumerable<object> GetEqualytyComponents()
        {
            yield return Value;
        }
    }
}
