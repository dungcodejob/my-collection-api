﻿using ErrorOr;

namespace MyCollection.Domain.Common.Errors
{
    public static partial class Errors
    {
        public static class Authentication
        {
            public static Error InvalidCredentials => Error.Conflict(code: "Auth.InvaildCred", description: "Invalid credentials");
        }
    }
}
