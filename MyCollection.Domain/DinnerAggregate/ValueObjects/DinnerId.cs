﻿using MyCollection.Domain.Common.Models;

namespace MyCollection.Domain.DinnerAggregate.ValueObjects

{
    public sealed class DinnerId : ValueObject
    {
        public Guid Value { get; }

        private DinnerId(Guid value)
        {
            Value = value;
        }

        public static DinnerId CreateUnique()
        {
            return new(Guid.NewGuid());
        }

        public override IEnumerable<object> GetEqualytyComponents()
        {
            yield return Value;
        }
    }
}
