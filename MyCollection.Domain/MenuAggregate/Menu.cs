﻿using MyCollection.Domain.Common.Models;
using MyCollection.Domain.Common.ValueObjects;
using MyCollection.Domain.DinnerAggregate.ValueObjects;
using MyCollection.Domain.HostAggregate.ValueObjects;
using MyCollection.Domain.MenuAggregate.Entities;
using MyCollection.Domain.MenuAggregate.ValueObjects;
using MyCollection.Domain.MenuReviewAggregate.ValueObjects;

namespace MyCollection.Domain.MenuAggregate
{
    public sealed class Menu : AggregateRoot<MenuId>
    {
        private readonly List<MenuSection> _sections = new();
        private readonly List<DinnerId> _dinnerIds = new();
        private readonly List<MenuReviewId> _menuReviewIds = new();


#pragma warning disable CS8618 // Non-nullable field must contain a non-null value when exiting constructor. Consider declaring as nullable.
        public Menu(MenuId id) : base(id) { }
#pragma warning restore CS8618 // Non-nullable field must contain a non-null value when exiting constructor. Consider declaring as nullable.

        private Menu(MenuId menuId, HostId hostId, string name, string description, AverageRating averageRating, List<MenuSection> sections) : base(menuId)
        {
            Name = name;
            Description = description;
            AverageRating = averageRating;
            HostId = hostId;
            _sections = sections;
        }
        public HostId HostId { get; private set; }
        public string Name { get; private set; }
        public string Description { get; private set; }
        public AverageRating AverageRating { get; private set; }


        public IReadOnlyList<MenuSection> Sections => _sections.AsReadOnly();
        public IReadOnlyList<DinnerId> DinnerIds => _dinnerIds.AsReadOnly();
        public IReadOnlyList<MenuReviewId> MenuReviewIds => _menuReviewIds.AsReadOnly();

        public DateTime CreatedDateTime { get; private set; }
        public DateTime UpdatedDateTime { get; private set; }

        public static Menu Create(HostId hostId, string name, string description, List<MenuSection>? sections = null)
        {

            return new Menu(MenuId.CreateUnique(), hostId, name, description, AverageRating.CreateNew(), sections ?? new());
        }
    }
}
