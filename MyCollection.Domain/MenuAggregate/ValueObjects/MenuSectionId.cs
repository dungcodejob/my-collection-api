﻿using MyCollection.Domain.Common.Models;

namespace MyCollection.Domain.MenuAggregate.ValueObjects
{
    public sealed class MenuSectionId : ValueObject
    {
        public Guid Value { get; }

        private MenuSectionId(Guid value)
        {
            Value = value;
        }

        public static MenuSectionId Create(Guid value)
        {
            return new MenuSectionId(value);
        }
        public static MenuSectionId CreateUnique()
        {
            return new(Guid.NewGuid());
        }

        public override IEnumerable<object> GetEqualytyComponents()
        {
            yield return Value;
        }
    }
}
