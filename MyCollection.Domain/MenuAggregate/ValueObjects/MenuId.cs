﻿using MyCollection.Domain.Common.Models;

namespace MyCollection.Domain.MenuAggregate.ValueObjects
{
    public sealed class MenuId : ValueObject
    {
        public Guid Value { get; }

        private MenuId(Guid value)
        {
            Value = value;
        }

        public static MenuId Create(Guid value)
        {
            return new MenuId(value);
        }

        public static MenuId CreateUnique()
        {
            return new MenuId(Guid.NewGuid());
        }

        public override IEnumerable<object> GetEqualytyComponents()
        {
            yield return Value;
        }
    }
}
