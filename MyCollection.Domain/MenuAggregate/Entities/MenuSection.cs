﻿using MyCollection.Domain.Common.Models;
using MyCollection.Domain.MenuAggregate.ValueObjects;

namespace MyCollection.Domain.MenuAggregate.Entities
{
    public sealed class MenuSection : Entity<MenuSectionId>
    {
#pragma warning disable CS8618 // Non-nullable field must contain a non-null value when exiting constructor. Consider declaring as nullable.
        public MenuSection(MenuSectionId id) : base(id) { }
#pragma warning restore CS8618 // Non-nullable field must contain a non-null value when exiting constructor. Consider declaring as nullable.
        private MenuSection(MenuSectionId menuSectionId, string name, string description, List<MenuItem> items) : base(menuSectionId)
        {
            Name = name;
            Description = description;
            _items = items;
        }
        private readonly List<MenuItem> _items = new();
        public string Name { get; }
        public string Description { get; }

        public IReadOnlyList<MenuItem> Items => _items.AsReadOnly();

        public static MenuSection Create(
            string name,
            string description,
            List<MenuItem>? items = null)
        {
            return new(MenuSectionId.CreateUnique(), name, description, items ?? new());
        }
    }
}
