﻿using MyCollection.Domain.Common.Models;

namespace MyCollection.Domain.HostAggregate.ValueObjects
{
    public sealed class HostId : ValueObject
    {
        //public Guid Value { get; }
        public string Value { get; }

        private HostId(string value)
        {
            Value = value;
        }

        public static HostId Create(string value)
        {
            return new(value);
        }
        //public static HostId CreateUnique()
        //{
        //    return new(Guid.NewGuid());
        //}


        public override IEnumerable<object> GetEqualytyComponents()
        {
            yield return Value;
        }
    }
}
