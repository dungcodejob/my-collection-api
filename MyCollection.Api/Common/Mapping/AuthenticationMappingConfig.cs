﻿using Mapster;
using MyCollection.Application.Authentication.Commands.Register;
using MyCollection.Application.Authentication.Common;
using MyCollection.Application.Authentication.Queries.Login;
using MyCollection.Contracts.Authentication;

namespace MyCollention.Api.Mapping
{
    public class AuthenticationMappingConfig : IRegister
    {
        public void Register(TypeAdapterConfig config)
        {
            config.NewConfig<RegisterRequestDto, RegisterCommand>();
            config.NewConfig<LoginRequestDto, LoginQuery>();
            config.NewConfig<AuthenticationResult, AuthenticationResponseDto>()
                .Map(destination => destination, source => source.User);

        }
    }
}
