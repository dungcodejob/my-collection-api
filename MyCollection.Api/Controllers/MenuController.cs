﻿using MapsterMapper;

using MediatR;

using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

using MyCollection.Application.Menus.Commands.CreateMenu;
using MyCollection.Contracts.Menus;

namespace MyCollention.Api.Controllers
{
    [Route("hosts/{hostId}/menus")]
    [ApiController]
    [Authorize]
    public class MenuController : ApiController
    {
        private readonly IMapper _mapper;
        private readonly ISender _mediator;

        public MenuController(IMapper mapper, ISender mediator)
        {
            _mapper = mapper;
            _mediator = mediator;
        }

        [HttpPost]
        public async Task<IActionResult> CreateMenu(string hostId, CreateMenuRequest request)
        {
            var command = _mapper.Map<CreateMenuCommand>((request, hostId));
            var createMenuResult = await _mediator.Send(command);
            return createMenuResult.Match(
                menu => Ok(_mapper.Map<MenuResponse>(menu)),
                error => Problem(error));
        }
    }
}
