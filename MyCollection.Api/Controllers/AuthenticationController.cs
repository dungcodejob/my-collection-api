﻿using MapsterMapper;

using MediatR;

using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

using MyCollection.Application.Authentication.Commands.Register;
using MyCollection.Application.Authentication.Queries.Login;
using MyCollection.Contracts.Authentication;

namespace MyCollention.Api.Controllers
{
    [Route("api/auth")]
    [ApiController]
    [AllowAnonymous]
    public class AuthenticationController : ApiController
    {
        private readonly ISender _mediator;
        private readonly IMapper _mapper;
        public AuthenticationController(ISender mediator, IMapper mapper)
        {
            _mediator = mediator;
            _mapper = mapper;
        }
        [HttpPost("register")]

        public async Task<IActionResult> Register(RegisterRequestDto request)
        {
            var command = _mapper.Map<RegisterCommand>(request);
            var authResult = await _mediator.Send(command);

            return authResult.Match(
                   authResult => Ok(_mapper.Map<AuthenticationResponseDto>(authResult)),
                   errors => Problem(errors)
                   );


        }


        [HttpPost("login")]
        public async Task<IActionResult> Login(LoginRequestDto request)
        {
            var query = _mapper.Map<LoginQuery>(request);
            var authResult = await _mediator.Send(query);

            return authResult.Match(
                  authResult => Ok(_mapper.Map<AuthenticationResponseDto>(authResult)),
                  errors => Problem(errors)
                  );

            //var response = new AuthenticationResponseDto(
            //    authResult.User.Id,
            //    authResult.User.FirstName,
            //    authResult.User.LastName,
            //    authResult.User.Username,
            //    authResult.Token);
            //return Ok(response);
        }
    }
}
