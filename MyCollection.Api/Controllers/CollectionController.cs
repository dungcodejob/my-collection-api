﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace MyCollention.Api.Controllers
{
    [Route("api/[controller]")]
    [Authorize]
    public class CollectionController : ApiController
    {
        [HttpGet]
        public IActionResult ListCollections()
        {

            var list = new List<Permission>();
            var p1 = new Permission();
            p1.Id = 1;
            p1.Name = "Nhập hàng hoá - thêm phiếu";
            list.Add(p1);

            var p2 = new Permission();
            p2.Id = 2;
            p2.Name = "Nhập hàng hoá - Xoá phiếu";
            list.Add(p2);


            var p3 = new Permission();
            p3.Id = 3;
            p3.Name = "Xuất bán hàng hoá - Thêm phiếu";
            list.Add(p3);

            var result = "SET IDENTITY_INSERT [dbo].[Permission] ON \n";

            var groupName = "";
            int groupId = 0;

            foreach (var item in list)
            {
                result += $"\t\tIF NOT EXISTS (SELECT * FROM [Permission] WHERE [Code] = {item.Id}) \n";

                var str = item.Name.Split(" - ");
                if (str.Length > 0)
                {
                    if (!groupName.Equals(str[0]))
                    {
                        groupName = str[0];
                        groupId = item.Id;
                    }

                    result += "\t\tBEGIN\n";
                    result += $"\t\tINSERT [dbo].[Permission] ([Code], [Name], [Description],[GroupPermissionId]) VALUES ({item.Id}, N'{str[1]}', N'{item.Description}', {groupId}) \n";
                    result += "\t\tEND \n \n";
                }
            }

            result += "SET IDENTITY_INSERT [dbo].[Permission] OFF \n";

            return Ok(result);
        }
    }

    public class Permission
    {
        public int Id { get; set; }
        public string Name { get; set; } = string.Empty;
        public string Description { get; set; } = string.Empty;
    }
}
