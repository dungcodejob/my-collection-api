﻿using Microsoft.AspNetCore.Mvc.Infrastructure;
using MyCollention.Api.Common.Mapping;
using MyCollention.Api.Errors;

namespace MyCollention.Api
{
    public static class DependecyInjection
    {
        public static IServiceCollection AddPresentation(this IServiceCollection services)
        {
            services.AddControllers();
            services.AddSingleton<ProblemDetailsFactory, MyCollectionProblemDetailsFactory>();
            services.AddMappings();

            return services;
        }
    }
}
