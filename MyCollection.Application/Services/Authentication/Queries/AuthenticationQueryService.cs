﻿using ErrorOr;

using MyCollection.Application.Authentication.Common;
using MyCollection.Application.Common.Interfaces.Authentication;
using MyCollection.Application.Common.Interfaces.Persistence;
using MyCollection.Domain.Common.Errors;
using MyCollection.Domain.UserAggregate.Entities;

namespace MyCollection.Application.Services.Authentication.Commands
{
    public class AuthenticationQueryService : IAuthenticationQueryService
    {
        private readonly IJwtTokenGenerator _jwtTokenGenerator;
        private readonly IUserRepository _userRepository;
        public AuthenticationQueryService(IJwtTokenGenerator jwtTokenGenerator, IUserRepository userRepository)
        {
            _jwtTokenGenerator = jwtTokenGenerator;
            _userRepository = userRepository;
        }
        public ErrorOr<AuthenticationResult> Login(string username, string password)
        {
            // 1. Validate the user exists
            if (_userRepository.GetUserByUsername(username) is not User user)
            {
                //throw new Exception("User with given username does not exist.");
                return Errors.Authentication.InvalidCredentials;
            }

            // 2. Validate the password is correct
            if (user.Password != password)
            {
                return Errors.Authentication.InvalidCredentials;
            }

            // 3. Create JWT token
            var token = _jwtTokenGenerator.GenerateToken(user);

            return new AuthenticationResult(user, token);
        }


    }
}
