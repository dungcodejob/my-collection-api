﻿using ErrorOr;
using MyCollection.Application.Authentication.Common;

namespace MyCollection.Application.Services.Authentication.Commands
{
    public interface IAuthenticationQueryService
    {
        ErrorOr<AuthenticationResult> Login(string username, string password);
    }
}
