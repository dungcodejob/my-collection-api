﻿using ErrorOr;

using MyCollection.Application.Authentication.Common;
using MyCollection.Application.Common.Interfaces.Authentication;
using MyCollection.Application.Common.Interfaces.Persistence;
using MyCollection.Domain.Common.Errors;
using MyCollection.Domain.UserAggregate.Entities;
namespace MyCollection.Application.Services.Authentication.Commands
{
    public class AuthenticationCommandService : IAuthenticationCommandService
    {
        private readonly IJwtTokenGenerator _jwtTokenGenerator;
        private readonly IUserRepository _userRepository;
        public AuthenticationCommandService(IJwtTokenGenerator jwtTokenGenerator, IUserRepository userRepository)
        {
            _jwtTokenGenerator = jwtTokenGenerator;
            _userRepository = userRepository;
        }


        public ErrorOr<AuthenticationResult> Register(string firstName, string lastName, string username, string password)
        {
            // 1. Validate the user doesn't exist
            if (_userRepository.GetUserByUsername(username) is not null)
            {
                return Errors.User.DuplicateUsername;
            }

            // 2. Create user (generate unique ID) & Persist to DB
            var user = new User
            {
                FirstName = firstName,
                LastName = lastName,
                Username = username,
                Password = password
            };
            _userRepository.Add(user);

            // 3. Create JWT token
            var token = _jwtTokenGenerator.GenerateToken(user);

            return new AuthenticationResult(user, token);

        }
    }
}
