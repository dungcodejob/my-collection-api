﻿using ErrorOr;
using MyCollection.Application.Authentication.Common;

namespace MyCollection.Application.Services.Authentication.Commands
{
    public interface IAuthenticationCommandService
    {
        ErrorOr<AuthenticationResult> Register(string firstName, string lastName, string username, string password);

    }
}
