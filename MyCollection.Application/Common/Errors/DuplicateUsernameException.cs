﻿using System.Net;

namespace MyCollection.Application.Common.Errors
{
    public class DuplicateUsernameException : Exception, IServiceException
    {
        public HttpStatusCode StatusCode => HttpStatusCode.Conflict;

        public string ErrorMessage => "Username already exists";
    }
}
