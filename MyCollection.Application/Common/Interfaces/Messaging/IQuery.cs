﻿using MediatR;

namespace MyCollection.Application.Common.Interfaces.Messaging
{
    public interface IQuery<out TResponse> : IRequest<TResponse>
    {
    }
}
