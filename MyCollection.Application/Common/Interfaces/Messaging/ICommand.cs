﻿using MediatR;

namespace MyCollection.Application.Common.Interfaces.Messaging
{
    public interface ICommand<out TResponse> : IRequest<TResponse>
    {
    }
}
