﻿using MyCollection.Domain.UserAggregate.Entities;

namespace MyCollection.Application.Common.Interfaces.Persistence
{
    public interface IUserRepository
    {
        User? GetUserByUsername(string username);
        void Add(User user);
    }
}
