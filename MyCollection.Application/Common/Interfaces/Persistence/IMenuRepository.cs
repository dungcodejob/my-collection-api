﻿using MyCollection.Domain.MenuAggregate;

namespace MyCollection.Application.Common.Interfaces.Persistence
{
    public interface IMenuRepository
    {
        void Add(Menu menu);
    }
}
