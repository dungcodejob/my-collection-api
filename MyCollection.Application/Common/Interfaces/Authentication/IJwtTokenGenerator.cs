﻿using MyCollection.Domain.UserAggregate.Entities;

namespace MyCollection.Application.Common.Interfaces.Authentication
{
    public interface IJwtTokenGenerator
    {
        string GenerateToken(User user);
    }
}
