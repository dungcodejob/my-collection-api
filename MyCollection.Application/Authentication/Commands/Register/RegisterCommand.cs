﻿using ErrorOr;
using MediatR;
using MyCollection.Application.Authentication.Common;

namespace MyCollection.Application.Authentication.Commands.Register
{
    public record RegisterCommand(
        string FirstName,
        string LastName,
        string Password,
        string Username) : IRequest<ErrorOr<AuthenticationResult>>;
}
