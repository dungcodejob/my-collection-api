﻿using MyCollection.Domain.UserAggregate.Entities;

namespace MyCollection.Application.Authentication.Common
{
    public record AuthenticationResult(
        User User,
        string Token);
}
