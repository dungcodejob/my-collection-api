﻿using ErrorOr;
using MediatR;
using MyCollection.Application.Authentication.Common;

namespace MyCollection.Application.Authentication.Queries.Login
{
    public record LoginQuery(
        string Username,
        string Password) : IRequest<ErrorOr<AuthenticationResult>>;
}
