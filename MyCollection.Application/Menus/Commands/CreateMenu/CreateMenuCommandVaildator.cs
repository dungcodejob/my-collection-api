﻿using FluentValidation;

namespace MyCollection.Application.Menus.Commands.CreateMenu
{
    public class CreateMenuCommandVaildator : AbstractValidator<CreateMenuCommand>
    {
        public CreateMenuCommandVaildator()
        {
            RuleFor(x => x.Name).NotEmpty();
            RuleFor(x => x.Description).NotEmpty();
            RuleFor(x => x.Sections).NotEmpty();
        }
    }
}
